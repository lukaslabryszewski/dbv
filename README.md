# Database version control, made easy!

**dbv.php** is a database version control web application featuring schema management, revision scripts, and more!

Check out the **[project website](http://dbv.vizuina.com)** for more details, features and documentation.

[![dbv.php](http://dbv.vizuina.com/img/screenshot-main.png)](http://dbv.vizuina.com)

dbv.php provides HTTP authentication that you can configure via the DBV_USERNAME and DBV_PASSWORD constants found in config.php. Make sure you change these default values.
Currently they are dbv:dbv

### Issues I found so far:

- Allows one database in the configuration at the moment. (Can be overcome by forking it on github.com and adding our own features)
- The revision is a sub-directory and it's name must be numerical (I propose a "YYYYMMDDHHMMSS" format and we should all agree on a single timezone PST or GMT)
- No Undo functionality (roll-back to a certain DB revision schema)
- No command-line tool to allow automatic build with Jenkins - can be added by us
- Keeps track of only the latest applied version number (numerically) so if a DB update gets merged from another branch, there is no way of knowing if it was applied.

Currenty this project works with the [API lukasDbv Branch](https://bitbucket.org/voxoxrocks/api/branch/lukasDbv) and is in it's testing phase.

## Local Vagrant Setup:

SSH to your voxox box and configure nginx.

```bash
$ sudo su -
$ nano /etc/nginx/conf.d/dbv.voxox.local.conf
```

```conf
server {
    listen 80;
    listen 8080;
    server_name dbv.voxox.local;
    root /www/dbv/;
    charset utf-8;

    location / {
        index index.html index.php;
        #try_files \$uri \$uri/ /index.php?\$query_string;
        try_files $uri /index.php$is_args$args;
        proxy_send_timeout   600;
        proxy_read_timeout   600;
        proxy_buffer_size    64k;
        proxy_buffers     16 32k;
        proxy_busy_buffers_size 64k;
        proxy_temp_file_write_size 64k;
        client_max_body_size 256M;
    }

    location ~ /\.ht {
        deny all;
    }

    location /nginx_status {
      stub_status on;
      access_log  off;
    }

    location ~ \.(hh|php)$ {
        fastcgi_read_timeout 600;
        fastcgi_keep_conn on;
        #fastcgi_pass      127.0.0.1:9000;
        fastcgi_pass unix:/var/run/php5-fpm.sock;
        fastcgi_index     index.php;
        fastcgi_param     SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include           fastcgi_params;
    }
}
```

```bash
$ nginx -s reload
```

Add to your local machine /etc/hosts file:

```hosts
10.20.1.153   dbv.voxox.local
```
